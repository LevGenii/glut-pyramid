/*
 * geometry.h
 *
 *  Created on: 06.12.2016
 *      Author: tsokalo
 */

#ifndef GEOMETRY_H_
#define GEOMETRY_H_

#include <stdint.h>
#include <math.h>
#include <iostream>
#include <vector>

#include "header.h"
extern double frameRate;

struct Coordinates {
	Coordinates() {
		x = 0;
		y = 0;
		z = 0;
	}
	Coordinates(float x, float y, float z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}
	Coordinates& operator=(const Coordinates& other) // copy assignment
	{
		if (this != &other) {
			this->x = other.x;
			this->y = other.y;
			this->z = other.z;
		}
		return *this;
	}
	float x;
	float y;
	float z;
};

struct Orientation {
	Orientation() {
		r = 1;
		y = 0;
	}
	Orientation(float r, float y) {
		this->r = r;
		this->y = y;
	}
	/*
	 * radius
	 */
	float r;
	/*
	 * vertical offset
	 */
	float y;
};

struct NodeGeometry {

	NodeGeometry(float r, float y, float c) :
		o(r, y), angle(5) {

		double yd = 2 * o.r;
		double xd = 2 * o.r;
		maxyshift = sin(DEG2RAD(angle)) * yd;
		maxzshift = cos(DEG2RAD(angle)) * yd;
		maxxshift = xd;
		counter = c;
		speed = SPEED_MANIPULATOR;

		std::cout << counter << std::endl;
	}

	void RecalcPosition() {

//		speed = SPEED_MANIPULATOR * frameRate / EXPECTED_FRAME_RATE;
//		counter = (double)counter * frameRate / EXPECTED_FRAME_RATE;

		c.x = cos(DEG2RAD(get_counter())) * maxxshift;
		c.y = sin(DEG2RAD(get_counter())) * maxyshift + o.y;
		c.z = sin(DEG2RAD(get_counter())) * maxzshift;

		inc_counter();
	}

	Coordinates c;
	Orientation o;

	double maxyshift;
	double maxzshift;
	double maxxshift;

private:

	void inc_counter() {
		counter = (counter > speed) ? 0 : (counter + 1);
	}
	double get_counter() {
		return (double) counter * 359.0 / speed;
	}

	int16_t counter;
	double angle;
	double speed;
};

struct NodeGeometryV : public std::vector<NodeGeometry>
{
	Coordinates GetCoordinates(int16_t i)
	{
		return this->at(i).c;
	}
};

#endif /* GEOMETRY_H_ */
