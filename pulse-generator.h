/*
 * pulse-generator.h
 *
 *  Created on: 06.12.2016
 *      Author: tsokalo
 */

#ifndef PULSEGENERATOR_H_
#define PULSEGENERATOR_H_

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <random>
#include <functional>

#include <GL/glut.h>
#include <GL/gl.h>

#include "globals.h"
#include "header.h"

typedef std::function<Coordinates(int16_t)> get_coord_func;

struct EdgePulse {

	EdgePulse(uint16_t src, uint16_t dst, get_coord_func get_coord) :
		step(PULSE_SMOOTHNESS) {
		this->src = src;
		this->dst = dst;
		this->get_coord = get_coord;
		p = 0;
	}
	Coordinates MakeStep() {

		p += step;
		Coordinates b = get_coord(src);
		Coordinates e = get_coord(dst);
		return Coordinates(b.x + (e.x - b.x) * p, b.y + (e.y - b.y) * p, b.z
				+ (e.z - b.z) * p);
	}
	bool IsFinished() {
		return (p >= 1);
	}

	uint16_t src;
	uint16_t dst;
	double p;
	double step;
	get_coord_func get_coord;
};

class Pulses: public std::vector<EdgePulse> {

	typedef std::vector<EdgePulse>::iterator pulse_it;
public:

	Pulses() {
		counter = 0;
	}
	~Pulses() {
	}

	void Draw() {

		for (pulse_it it = this->begin(); it != this->end();) {

			DrawPulse(it->MakeStep());

			if (it->IsFinished()) {
				this->erase(it, it + 1);
			} else {
				it++;
			}
		}
	}

private:

	void DrawPulse(Coordinates c) {

//		if(++counter < 10)return;
//
//		counter = 0;

		glDepthMask(GL_FALSE);

		glEnable(GL_LINE_SMOOTH);

		double max_o = 0.5;
		GLfloat amb[] = { 0.0, 1.0, 0.0, max_o };
		GLfloat dif[] = { 0.5, 0.5, 0.5, max_o };
		glMaterialfv(GL_FRONT, GL_AMBIENT, amb);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, dif);

		glPushMatrix();
		glTranslatef(c.x, c.y, c.z);

		uint16_t s = 10;
		double r = 0.01;
		for (uint16_t i = 0; i < s; i++) {
			glMaterialfv(GL_FRONT, GL_AMBIENT, amb);
			glMaterialfv(GL_FRONT, GL_DIFFUSE, dif);
			glutSolidSphere(r, 50, 50);

			amb[3] -= 1.0 / (double) s * max_o;
			dif[3] -= 1.0 / (double) s * max_o;
			r += (0.00025 * (double) i + 0.000125 * (double) i * i);
		}

		glPopMatrix();

		glDepthMask(GL_TRUE);
	}

	int16_t counter;

};

class PulseGenerator: public Pulses {

public:

	PulseGenerator(get_coord_func get_coord) :
		p(PULSE_FREQUENCY), gen1(rd()), gen2(rd()), d(0, NUM_NODES - 1), r(0, 1) {
		this->get_coord = get_coord;
	}
	~PulseGenerator() {
	}

	void GenerateRandom() {
		if (p > r(gen2)) {

			auto src = d(gen1);
			uint16_t dst = 0;
			do {
				dst = d(gen2);
			} while (src == dst);

			std::cout << "Create pulse between " << src << " and " << dst
					<< std::endl;

			this->push_back(EdgePulse(src, dst, get_coord));
		}
	}
	void GenerateFrom(uint16_t src) {
		for (uint16_t i = 0; i < NUM_NODES; i++) {
			if (i == src)
				continue;
			this->push_back(EdgePulse(src, i, get_coord));
		}
	}
private:
	const double p;
	std::random_device rd;
	std::mt19937 gen1;
	std::mt19937 gen2;
	std::uniform_int_distribution<int16_t> d;
	std::uniform_real_distribution<double> r;
	get_coord_func get_coord;

};

#endif /* PULSEGENERATOR_H_ */
