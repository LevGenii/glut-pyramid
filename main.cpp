/*
 * Ievgenii Tsokalo
 */

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <GL/glut.h>
#include <GL/gl.h>

#include <random>

#include "glu-events.h"

void InitLight() {

	// Enable lighting
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	GLfloat qaAmbientLight[] = { 1, 1, 1, 1.0 };
	GLfloat qaDiffuseLight[] = { 0.5, 0.5, 0.5, 1.0 };
	GLfloat qaLightPosition1[] = { 1, 1, 1, 1 };
	GLfloat qaLightPosition2[] = { -1, 0, 100, 10 };

	// Set lighting intensity and color
	glLightfv(GL_LIGHT0, GL_AMBIENT, qaAmbientLight);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, qaDiffuseLight);

	// Set the light position
	glLightfv(GL_LIGHT0, GL_POSITION, qaLightPosition1);
	glLightfv(GL_LIGHT1, GL_POSITION, qaLightPosition2);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}

int main(int argc, char **argv) {

	std::srand(time(0));
	lastRendering = std::chrono::system_clock::now();

	InitDrawing();

	// init GLUT and create window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA | GLUT_ALPHA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(1000, 800);
	glutCreateWindow("Lighthouse3D - GLUT Tutorial");

	InitLight();

	// register callbacks
	glutDisplayFunc(RenderScene);
	glutReshapeFunc(ReshapeScene);
	glutIdleFunc(RenderScene);

	glutSpecialFunc(PressKey);
	glutMouseFunc(OnMouseClick);

	// OpenGL init
	glEnable(GL_DEPTH_TEST);

	// enter GLUT event processing cycle
	glutMainLoop();

	return 1;
}

