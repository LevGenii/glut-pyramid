/*
 * globals.cpp
 *
 *  Created on: 06.12.2016
 *      Author: tsokalo
 */

#include <functional>

#include "globals.h"
#include "geometry.h"

bool fullscreen = false;
float ratio = 1;
double cam_xmin, cam_xmax, cam_ymin, cam_ymax;
NodeGeometryV geom;
PulseGenerator pulseGenerator(std::bind(&NodeGeometryV::GetCoordinates, &geom, std::placeholders::_1));
double frameRate = 40;
std::chrono::time_point<std::chrono::system_clock> lastRendering;
int16_t aveWinFrameRate = 0;
