/*
 * globals.h
 *
 *  Created on: 06.12.2016
 *      Author: tsokalo
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <ctime>
#include <chrono>

#include "geometry.h"
#include "pulse-generator.h"

extern bool fullscreen;
extern float ratio;
extern double cam_xmin, cam_xmax, cam_ymin, cam_ymax;
extern NodeGeometryV geom;
extern PulseGenerator pulseGenerator;
//extern double frameRate;
extern std::chrono::time_point<std::chrono::system_clock> lastRendering;
extern int16_t aveWinFrameRate;

#endif /* GLOBALS_H_ */
