/*
 * draw.h
 *
 *  Created on: 06.12.2016
 *      Author: tsokalo
 */

#ifndef DRAW_H_
#define DRAW_H_

#include <vector>
#include <random>

#include <GL/glut.h>
#include <GL/gl.h>

#include "header.h"
#include "globals.h"

void InitDrawing() {

	double r, y;
	double l_r = 0.11, u_r = 0.4, l_y = -0.4, u_y = 0.6, l_c = 0, u_c =
			SPEED_MANIPULATOR - 1;
	std::vector<double> rs, ys, cs;
	for (uint16_t i = 0; i < NUM_NODES; i++) {
		rs.push_back((u_r - l_r) / (double) NUM_NODES * i + l_r);
		ys.push_back((u_y - l_y) / (double) NUM_NODES * i + l_y);
		cs.push_back((u_c - l_c) / (double) NUM_NODES * i + l_c);
	}

	typedef std::vector<double>::iterator d_it;
	auto swap_el = [](d_it i, d_it j)
	{
		auto v = *i;
		*i = *j;
		*j = v;
	};;

	swap_el(rs.begin(), rs.begin() + 6);
	swap_el(rs.begin() + 2, rs.begin() + 4);
	swap_el(rs.begin() + 3, rs.begin() + 8);

	swap_el(ys.begin() + 1, ys.begin() + 6);
	swap_el(ys.begin() + 2, ys.begin() + 5);
	swap_el(ys.begin() + 2, ys.begin() + 7);

	swap_el(cs.begin() + 5, cs.begin() + 6);
	swap_el(cs.begin() + 3, cs.begin() + 4);
	swap_el(cs.begin() + 1, cs.begin() + 8);

	for (uint16_t i = 0; i < NUM_NODES; i++) {

		geom.push_back(NodeGeometry(*(rs.begin() + i), *(ys.begin() + i),
				*(cs.begin() + i)));
	}
}

void DrawEdge(Coordinates i, Coordinates j) {

	glDepthMask(GL_FALSE);

	glEnable(GL_LINE_SMOOTH);

	GLfloat amb[] = { 0, 1, 0, 0.5 };
	GLfloat dif[] = { 0.5, 1.0, 0.5, 0.5 };
	//
	glLineWidth(1);
	glMaterialfv(GL_FRONT, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, dif);
	glBegin(GL_LINES);
	glColor4f(0, 1, 0, 0.5);
	glVertex3f(i.x, i.y, i.z);
	glVertex3f(j.x, j.y, j.z);
	glEnd();

	amb[3] = 0.2;
	dif[3] = 0.2;
	glLineWidth(10);
	glMaterialfv(GL_FRONT, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, dif);
	glBegin(GL_LINES);
	glColor4f(0, 1, 0, 0.5);
	glVertex3f(i.x, i.y, i.z);
	glVertex3f(j.x, j.y, j.z);
	glEnd();

	glDepthMask(GL_TRUE);
}

void ConnectNodes() {
	for (int16_t i = 0; i < NUM_NODES; i++) {
		for (int16_t j = 0; j < NUM_NODES; j++) {
			if (i == j) {
				continue;
			}
			DrawEdge(geom.at(i).c, geom.at(j).c);
		}
	}
}

void DrawNodes() {
	GLfloat amb[] = { 0.3, 0.3, 0.3, 1.0 };
	GLfloat dif[] = { 0.5, 0.5, 0.5, 1.0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, dif);
	for (uint16_t i = 0; i < NUM_NODES; i++) {
		glPushMatrix();
		glTranslatef(geom.at(i).c.x, geom.at(i).c.y, geom.at(i).c.z);
		glutSolidSphere(NODE_SIZE_GEOM, 50, 50);
		glPopMatrix();
	}
}
#endif /* DRAW_H_ */
