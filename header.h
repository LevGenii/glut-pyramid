/*
 * header.h
 *
 *  Created on: 06.12.2016
 *      Author: tsokalo
 */

#ifndef HEADER_H_
#define HEADER_H_

#define SPEED_MANIPULATOR	2000.0
#define EXPECTED_FRAME_RATE	25.0
#define REACTION_ON_LOAD	20.0
#define NUM_NODES 			9
#define NODE_SIZE_GEOM		0.07
#define PULSE_SMOOTHNESS	0.02
/*
 * probability to generate a pulse per rendering event per node
 */
#define PULSE_FREQUENCY		0.1 / (double) NUM_NODES

#define DEG2RAD(X) X * 2 * 3.14159 / 360.0

#endif /* HEADER_H_ */
