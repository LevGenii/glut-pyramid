/*
 * glu-events.h
 *
 *  Created on: 06.12.2016
 *      Author: tsokalo
 */

#ifndef GLUEVENTS_H_
#define GLUEVENTS_H_

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <random>
#include <ctime>
#include <chrono>

#include <GL/glut.h>
#include <GL/gl.h>

#include "draw.h"
#include "globals.h"

void RenderScene(void) {

	if (aveWinFrameRate++ > REACTION_ON_LOAD) {
		std::chrono::time_point<std::chrono::system_clock> now =
				std::chrono::system_clock::now();

		double elapsed_milliseconds = std::chrono::duration_cast<
				std::chrono::milliseconds>(now - lastRendering).count();
		lastRendering = now;
		frameRate = REACTION_ON_LOAD / elapsed_milliseconds * 1000;
		aveWinFrameRate = 0;
		std::cout << "Frame rate: " << frameRate << std::endl;
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(80.0f, ratio, 0.1f, 100.0f);
	glTranslatef(0, 0, -2);

	DrawNodes();

	glPushMatrix();

	ConnectNodes();

	pulseGenerator.GenerateRandom();
	pulseGenerator.Draw();

	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	//
	glutSwapBuffers();

	for (auto &g : geom) {
		g.RecalcPosition();
	};;
}

void ReshapeScene(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;

	ratio = (double) w / (double) h;

	if (w > h) {
		cam_xmin = -double(w) / h;
		cam_xmax = double(w) / h;
		cam_ymin = -1.;
		cam_ymax = 1.;
	} else {
		cam_xmin = -1.;
		cam_xmax = 1.;
		cam_ymin = -double(h) / w;
		cam_ymax = double(h) / w;
	}

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	gluPerspective(80.0f, ratio, 0.1f, 100.0f);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void PressKey(int key, int x, int y) {
	char *name;

	switch (key) {
	case 27: {

		exit(0);
		break;
	}
	case GLUT_KEY_F2:
		name = "F2";
		if (!fullscreen) {
			glutFullScreen();
			fullscreen = true;
		} else if (fullscreen) {
			glutReshapeWindow(640, 480);
			glutPositionWindow(0, 0);
			fullscreen = false;
		}
		break;
	default:
		name = "UNKONW";
		break;
	}
	printf("special: %s %d,%d\n", name, x, y);
}

void OnMouseClick(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		std::vector<int16_t> ids;

		for (uint16_t i = 0; i < NUM_NODES; i++) {

			double t; // Lirping parameter

			t = double(x) / glutGet(GLUT_WINDOW_WIDTH);
			double cx = cam_xmin + t * (cam_xmax - cam_xmin);

			t = double(y) / glutGet(GLUT_WINDOW_HEIGHT);
			double cy = cam_ymax + t * (cam_ymin - cam_ymax);

			double s = NODE_SIZE_GEOM;
			auto r = geom.at(i).c;

			if (fabs(cx - r.x) < s && fabs(cy - r.y) < s) {
				std::cout << "COORD: " << cx << "," << cy << std::endl;
				std::cout << "Node: " << r.x << "," << r.y << "," << r.z
						<< std::endl;
				ids.push_back(i);
			}
		}
		double maxz = 0;
		int16_t j = 0;
		for (uint16_t i = 0; i < ids.size(); i++) {
			if (geom.at(ids.at(i)).c.z > maxz) {
				maxz = geom.at(ids.at(i)).c.z;
				j = i;
			}
		}
		if (!ids.empty())
			pulseGenerator.GenerateFrom(ids.at(j));
	}
}

#endif /* GLUEVENTS_H_ */
